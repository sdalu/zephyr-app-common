/*
 * Copyright (c) 2019  --  Stephane D'Alu
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr.h>
#include <shell/shell.h>
#include <stdlib.h>
#include <device.h>
#include <drivers/flash.h>
#include <storage/flash_map.h>
#include <fs/nvs.h>
#include <strings.h>

#include "info-str.h"
#include "info-str/common.h"
#include "info-str/zephyr.h"



#define FLASH_CTRL_NODE  DT_CHOSEN(zephyr_flash_controller)
#define FLASH_CTRL_LABEL DT_LABEL(FLASH_NODE)


int
info_str_cmd_os(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);

    shell_print(shell, "Version   : %s ", INFO_STR_KERNEL_VERSION);
    shell_print(shell, "Ticks/sec : %d", CONFIG_SYS_CLOCK_TICKS_PER_SEC);
    shell_print(shell, "SysWorkQ  : stack-size=%d, priority=%d",
		CONFIG_SYSTEM_WORKQUEUE_STACK_SIZE,
		CONFIG_SYSTEM_WORKQUEUE_PRIORITY);

#if FLASH_AREA_LABEL_EXISTS(storage)
    shell_print(shell, "Partition : storage[%d] = %ld @ %lx",
		FLASH_AREA_ID(storage), FLASH_AREA_SIZE(storage),
		FLASH_AREA_OFFSET(storage));
#endif

#if defined(INFO_STR_GUI_VERSION)
    shell_print(shell, "GUI       : %s", INFO_STR_GUI_VERSION);
#endif
	
    return 0;
}


int
info_str_cmd_build(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);
	
    shell_print(shell, "Compiler  : %s (%s)", INFO_STR_COMPILER_NAME,
		                              INFO_STR_COMPILER_VERSION);
    shell_print(shell, "Build Time: %s",      INFO_STR_BUILD_TIME);
#if defined(INFO_STR_DEBUG_OPTIONS)
    shell_print(shell, "Debug opts: %s",      INFO_STR_DEBUG_OPTIONS);
#endif
#if defined(INFO_STR_DEBUGGER_SUPPORT)
    shell_print(shell, "Debugger  : %s",      INFO_STR_DEBUGGER_SUPPORT);
#endif
#if defined(INFO_STR_STACK_GUARD)
    shell_print(shell, "Stack     : %s",      INFO_STR_STACK_GUARD);
#endif

#if defined(INFO_STR_APP_VERSION)
    shell_print(shell, "Version   : " INFO_STR_APP_VERSION);
#endif
#if defined(INFO_STR_APP_VERSION_COMMIT_ID_FULL)
    shell_print(shell, "Git commit: " INFO_STR_APP_VERSION_COMMIT_ID_FULL);
#endif
    return 0;
}


 int
info_str_cmd_mcu(const struct shell *shell, size_t argc, char **argv)
{
    ARG_UNUSED(argc);
    ARG_UNUSED(argv);


    /* Nordic specfic */
#if defined(NRF_FICR)
    shell_print(shell, "Device ID : 0x%08x%08x",
		NRF_FICR->DEVICEID[1], NRF_FICR->DEVICEID[0] << 0);
    shell_print(shell, "Model     : NRF %x (%c%c%c%c)", NRF_FICR->INFO.PART,
		(char)(NRF_FICR->INFO.VARIANT >> 24),
		(char)(NRF_FICR->INFO.VARIANT >> 16),
		(char)(NRF_FICR->INFO.VARIANT >>  8),
		(char)(NRF_FICR->INFO.VARIANT >>  0));
#endif
#if defined(NRF_UICR)
    if (NRF_UICR->PSELRESET[0] == NRF_UICR->PSELRESET[1]) {
#if defined(CONFIG_GPIO_AS_PINRESET)
	shell_print(shell, "Reset     : PIN %d (%s) [build option]",
#else
	shell_print(shell, "Reset     : PIN %d (%s)",
#endif
		    NRF_UICR->PSELRESET[0],
		    (NRF_UICR->PSELRESET[0] & (1 << 31)) ? "Disconnected"
		                                         : "Connected");
    } else {
	shell_print(shell, "Reset     : <UICR register corrupted>");
    }
#endif
#if defined(NRF_FICR)
    shell_print(shell, "RAM       : %4d kB", NRF_FICR->INFO.RAM);
    shell_print(shell, "Flash     : %4d kB", NRF_FICR->INFO.FLASH);
#endif
    
    shell_print(shell, "Byte-order: %s", INFO_STR_BYTE_ORDER "-endian");
    return 0;
}

