#ifndef __INFO_STR__H
#define __INFO_STR__H

#include <zephyr.h>

#if defined(CONFIG_SHELL)
#include <shell/shell.h>

int info_str_cmd_os(const struct shell *shell, size_t argc, char **argv);
int info_str_cmd_build(const struct shell *shell, size_t argc, char **argv);
int info_str_cmd_mcu(const struct shell *shell, size_t argc, char **argv);

#endif

#endif
