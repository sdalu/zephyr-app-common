#ifndef __INFO_STR__COMMON__H
#define __INFO_STR__COMMON__H

#define INFO_STR_HELPER(x) #x
#define INFO_STR(x) INFO_STR_HELPER(x)


/* Byte order string
 */
#if   __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define INFO_STR_BYTE_ORDER "little"
#elif __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
#define INFO_STR_BYTE_ORDER "big"
#else
#error "Unknown byte-order variant"
#endif


/* Compiler name
 */
#if defined(__clang__)
#define INFO_STR_COMPILER_NAME    "Clang/LLVM"
#elif defined(__ICC) || defined(__INTEL_COMPILER)
#define INFO_STR_COMPILER_NAME    "Intel ICC/ICPC"
#elif defined(__GNUC__) || defined(__GNUG__)
#define INFO_STR_COMPILER_NAME    "GNU GCC/G++"
#elif defined(__HP_cc) || defined(__HP_aCC)
#define INFO_STR_COMPILER_NAME    "HP C/aC++"
#elif defined(__IBMC__) || defined(__IBMCPP__)
#define INFO_STR_COMPILER_NAME    "IBM XL C/C++"
#elif defined(_MSC_VER)
#define INFO_STR_COMPILER_NAME    "Microsoft Visual Studio"
#elif defined(__PGI)
#define INFO_STR_COMPILER_NAME    "Portland Group PGCC/PGCPP"
#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
#define INFO_STR_COMPILER_NAME    "Oracle Solaris Studio"
#else
#define INFO_STR_COMPILER_NAME    "Unknown"
#endif

/* Compiler version
 */
#if defined(__clang__)
#define INFO_STR_COMPILER_VERSION    INFO_STR(__clang_major__) "." INFO_STR(__clang_minor__) "." INFO_STR(__clang_patchlevel__)
#elif defined(__ICC) || defined(__INTEL_COMPILER)
#define INFO_STR_COMPILER_VERSION    __INTEL_COMPILER
#elif defined(__GNUC__) || defined(__GNUG__)
#define INFO_STR_COMPILER_VERSION    INFO_STR(__GNUC__) "." INFO_STR(__GNUC_MINOR__)  "." INFO_STR(__GNUC_PATCHLEVEL__)
#elif defined(__HP_cc) || defined(__HP_aCC)
#define INFO_STR_COMPILER_VERSION    __HP_cc
#elif defined(__IBMC__) || defined(__IBMCPP__)
#define INFO_STR_COMPILER_VERSION    __xlc__
#elif defined(_MSC_VER)
#define INFO_STR_COMPILER_VERSION    _MSC_FULL_VER
#elif defined(__PGI)
#define INFO_STR_COMPILER_VERSION    INFO_STR(__PGIC__) "." INFO_STR(__PGIC_MINOR__)  "." INFO_STR(__PGIC_PATCHLEVEL__)
#elif defined(__SUNPRO_C) || defined(__SUNPRO_CC)
#define INFO_STR_COMPILER_VERSION    __SUNPRO_C
#else
#define INFO_STR_COMPILER_VERSION    "?.?.?"
#endif


/* Build time
 */
#define INFO_STR_BUILD_TIME __DATE__ " - " __TIME__


/* Application version
 */
#ifndef INFO_STR_APP_VERSION
#if defined(INFO_STR_APP_VERSION_NICKNAME)
#define INFO_STR_APP_VERSION INFO_STR_APP_VERSION_NICKNAME
#else
#define INFO_STR_APP_VERSION "< n/a >"
#endif
#endif

#ifndef INFO_STR_APP_VERSION_ID
#if   defined(INFO_STR_APP_VERSION_COMMIT_ID)
#define INFO_STR_APP_VERSION_ID INFO_STR_APP_VERSION_COMMIT_ID
#elif defined(INFO_STR_APP_VERSION_COMMIT_ID_FULL)
#define INFO_STR_APP_VERSION_ID INFO_STR_APP_VERSION_COMMIT_ID_FULL
#elif defined(INFO_STR_APP_VERSION_COMMIT_ID_SHORT)
#define INFO_STR_APP_VERSION_ID INFO_STR_APP_VERSION_COMMIT_ID_SHORT
#else
#define INFO_STR_APP_VERSION_ID "< n/a >"
#endif
#endif

#endif
