#ifndef __INFO_STR___ZEPHYR__H
#define __INFO_STR___ZEPHYR__H

#include <zephyr.h>
#include <version.h>

/* Kernel
 */
#define INFO_STR_KERNEL_VERSION "Zephyr " KERNEL_VERSION_STRING


/* GUI
 */
#if defined(CONFIG_LVGL)
#include <lvgl.h>

#define INFO_STR_GUI_VERSION					\
    "LVGL "							\
    INFO_STR(LVGL_VERSION_MAJOR)  "."				\
    INFO_STR(LVGL_VERSION_MINOR)  "."				\
    INFO_STR(LVGL_VERSION_PATCH)
#endif


/* Debugger support
 */
#if   defined(CONFIG_USE_SEGGER_RTT ) && \
      defined(CONFIG_RTT_CONSOLE    )
#define INFO_STR_SEGGER_SUPPORT "segger(RTT+console)"
#elif defined(CONFIG_USE_SEGGER_RTT )
#define INFO_STR_SEGGER_SUPPORT "segger(RTT)"
#endif

#if   defined(CONFIG_OPENOCD_SUPPORT)
#define INFO_STR_OPENOCD_SUPPORT "openocd"
#endif

#if defined(INFO_STR_OPENOCD_SUPPORT) && \
    defined(INFO_STR_SEGGER_SUPPORT )
#define INFO_STR_DEBUGGER_SUPPORT				\
    INFO_STR_OPENOCD_SUPPORT ", " INFO_STR_SEGGER_SUPPORT
#elif defined(INFO_STR_OPENOCD_SUPPORT)
#define INFO_STR_DEBUGGER_SUPPORT INFO_STR_OPENOCD_SUPPORT
#elif defined(INFO_STR_SEGGER_SUPPORT )
#define INFO_STR_DEBUGGER_SUPPORT INFO_STR_SEGGER_SUPPORT
#endif


/* Debug options
 */
#if   defined(CONFIG_NO_OPTIMIZATIONS) &&				\
      defined(CONFIG_DEBUG           ) &&				\
      defined(CONFIG_ASSERT          )
#define _INFO_STR_DEBUG_OPTIONS "no-optimizations, debug, assert"
#elif defined(CONFIG_NO_OPTIMIZATIONS) &&				\
      defined(CONFIG_DEBUG           )
#define _INFO_STR_DEBUG_OPTIONS "no-optimizations, debug"
#elif defined(CONFIG_NO_OPTIMIZATIONS) &&	\
      defined(CONFIG_ASSERT          )
#define _INFO_STR_DEBUG_OPTIONS "no-optimizations, assert"
#elif defined(CONFIG_DEBUG           ) &&				\
      defined(CONFIG_ASSERT          )
#define _INFO_STR_DEBUG_OPTIONS "debug, assert"
#elif defined(CONFIG_NO_OPTIMIZATIONS)
#define _INFO_STR_DEBUG_OPTIONS "no-optimizations"
#elif defined(CONFIG_DEBUG           )
#define _INFO_STR_DEBUG_OPTIONS "debug"
#elif defined(CONFIG_ASSERT          )
#define _INFO_STR_DEBUG_OPTIONS "assert"
#endif

#if   defined(_INFO_STR_DEBUG_OPTIONS      ) &&				\
      defined(_INFO_STR_DEBUG_OPTIONS_EXTRA)
#define INFO_STR_DEBUG_OPTIONS		_INFO_STR_DEBUG_OPTIONS ", "    \
                                        _INFO_STR_DEBUG_OPTIONS_EXTRA
#elif defined(_INFO_STR_DEBUG_OPTIONS      )
#define INFO_STR_DEBUG_OPTIONS		_INFO_STR_DEBUG_OPTIONS
#elif defined(_INFO_STR_DEBUG_OPTIONS_EXTRA)
#define INFO_STR_DEBUG_OPTIONS		_INFO_STR_DEBUG_OPTIONS_EXTRA
#endif


/* Stack guard
 */
#if   defined(CONFIG_STACK_CANARIES     ) &&				\
      defined(CONFIG_MPU_STACK_GUARD    ) &&				\
      defined(CONFIG_HW_STACK_PROTECTION)
#define INFO_STR_STACK_GUARD "canaries, mpu-guard, hw-protection"
#elif defined(CONFIG_STACK_CANARIES     ) &&				\
      defined(CONFIG_MPU_STACK_GUARD    )
#define INFO_STR_STACK_GUARD "canaries, mpu-guard"
#elif defined(CONFIG_STACK_CANARIES     ) &&				\
      defined(CONFIG_HW_STACK_PROTECTION)
#define INFO_STR_STACK_GUARD "canaries, hw-protection"
#elif defined(CONFIG_MPU_STACK_GUARD    ) &&				\
      defined(CONFIG_HW_STACK_PROTECTION)
#define INFO_STR_STACK_GUARD "mpu-guard, hw-protection"
#elif defined(CONFIG_STACK_CANARIES     )
#define INFO_STR_STACK_GUARD "canaries"
#elif defined(CONFIG_MPU_STACK_GUARD    )
#define INFO_STR_STACK_GUARD "mpu-guard"
#elif defined(CONFIG_HW_STACK_PROTECTION)
#define INFO_STR_STACK_GUARD "hw-protection"
#endif

#endif
